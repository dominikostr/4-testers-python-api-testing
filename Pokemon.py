import requests

def test_if_json_is_empty():
    response = requests.get('https://pokeapi.co/api/v2/pokemon')
    body = response.json()
    assert len(body['results']) > 0
    assert body["results"] != []
    assert response.status_code == 200
    assert body["count"] == 1279


def test_response_time_under_1s():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    assert response.elapsed.total_seconds() < 1
    print(response.elapsed.total_seconds())
    response_time_ms = response.elapsed.microseconds // 1000
    assert response_time_ms < 1000

    response_size_kb = len(response.content) / 1000
    assert response_size_kb < 100

def test_limit_and_offset():
    params = {
            "offset": 20,
            "limit": 10
        }
    response = requests.get('https://pokeapi.co/api/v2/pokemon', params=params)
    body = response.json()
    assert len(body['results']) == params['limit']













