import requests
class GoRESTHandler:
    base_url = 'https://gorest.co.in/public/v2/'
    users_endpoint = '/users'

    headers = {
        'Authorization': 'Bearer 3b9c6423a17ce079627605c09abb6cfc59fec853bfeb5e050c4c3ccba3375dbd'
    }

    def crate_user(self, user_data):
        response = requests.post(self.base_url+self.users_endpoint, headers=self.headers, json=user_data)
        assert response.status_code == 201
        return response

    def get_user(self, id):
        response = requests.get(self.base_url+self.users_endpoint+f'/{id}', headers=self.headers)
        assert response.status_code == 200
        return response

    def update_user(self, id, user_data):
        response = requests.patch(self.base_url+self.users_endpoint+f'/{id}', headers=self.headers, json=user_data)
        assert response.status_code == 200
        return response



    def delete_user(self, id):
        response = requests.delete(self.base_url+self.users_endpoint+f'/{id}', headers=self.headers)
        assert response.status_code == 204
        return response


