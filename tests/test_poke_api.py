import requests
from utils.pokeapi_handler import PokeAPIHandler

pokeapi_handler = PokeAPIHandler()





def test_pagination():
    params = {
        "limit": 11,
        "offset": 21
    }

    response = pokeapi_handler.get_list_of_pokemons(params)
    body = response.json()

    assert len(body["results"]) == params["limit"]
    assert body["results"][0]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset']+1}/"
    assert body["results"][-1]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset'] + params['limit']}/"

def test_pokemon_shape_list():
    response = pokeapi_handler.get_shape_of_pokemons()
    body = response.json()
    assert body["count"] == len(body["results"])

    third_shape_name = body["results"][2]["name"]
    response_name = pokeapi_handler.get_pokemon_shape_name(third_shape_name)
    body_name = response_name.json()
    assert body_name["id"] == 3
