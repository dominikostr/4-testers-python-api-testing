from utils.gorest_handler import GoRESTHandler
from faker import Faker

gorest_handler = GoRESTHandler()


def test_create_user():
    fake = Faker()
    user_data = {
        'name': 'Vidur Gowda',
        'email': fake.email(),
        'gender': 'female',
        'status': 'active'
    }
    body = gorest_handler.crate_user(user_data).json()
    print(body)
    assert 'id' in body
    print(user_data)
    body_user = gorest_handler.get_user(body['id']).json()
    assert body['email'] == body_user['email']

    id = body['id']
    updated_user_data = {
        'name': user_data['name'],
        'email': fake.email(),
        'gender': user_data['gender'],
        'status': 'inactive'
    }
    body_update = gorest_handler.update_user(id, updated_user_data).json()
    print(body)
    assert body_update['name'] == body['name']
    assert body_update['status'] == 'inactive'

    response = gorest_handler.delete_user(id)
    assert response.status_code == 204

